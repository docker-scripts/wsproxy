include(bookworm)

RUN apt update && apt --yes upgrade &&\
    apt --yes install \
        apache2 wget fail2ban net-tools

### install certbot (for getting ssl certs with letsencrypt)
RUN DEBIAN_FRONTEND=noninteractive apt install --yes certbot
