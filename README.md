# About Web Server Proxy

If we want to host several domains/subdomains on the same webserver
we can use *name-based virtual hosting*. If we need to host these
domains/subdomains in different webservers, each one in its own
docker container, there is a problem because the ports 80/443 can
be used (exposed to the host) only by one of the containers.

In such a case the *Reverse Proxy* module of apache2 comes to the
rescue. We can have a docker container with apache2 that forwards
all the http requests to the other containers (webservers), behaving
like a kind of http gateway or hub. This is what **wsproxy** does.

# Installation

 + Install `ds` (docker scripts):
   https://gitlab.com/docker-scripts/ds#installation

 + Get the scripts: `ds pull wsproxy`

 + Initialize a directory: `ds init wsproxy @revproxy`

 + Customize the settings: `cd /var/ds/revproxy/; vim settings.sh`  
   Modify at least `SSL_CERT_EMAIL`.

 + Make the container: `ds make`


# Commands

    domains-add <domain> [<alias> ...] [-c | --ssl-cert]
         Add a configuration file for the given domain (aliases are optional).
         If the option '--ssl-cert' is given, get a certificate as well.

    domains-rm <domain> [<domain> ...] [-c | --ssl-cert]
         Remove one or more domains from the configuration of the web proxy.
         If the option '--ssl-cert' is given, remove the corresponding certificates as well.

    get-ssl-cert <domain>... [-t,--test]
         Get free SSL certificates from letsencrypt.org

    del-ssl-cert <domain>
         Delete the SSL cert for the given domain.

On the directory of each application/container you can use the global
command `ds revproxy` to manage its domain and ssl-cert:

    revproxy [ ls | add | rm | ssl-cert ]
        Manage the domain of the container.


# Enable Proxy Protocol

If the proxy server is itself behind a SNI proxy (which forwards HTTPS
requests), and the SNI proxy has activated the Proxy Protocol (which
is used to forward the real IP of the client to our proxy), we also
need to enable the proxy protocol on our proxy.

We can do this by uncommenting the line `ENABLE_PROXY_PROTOCOL=true`
on `settings.sh`, before building the container.

By the way, to disable it, we can just rename
`conf.d/proxy_protocol.conf` to `conf.d/proxy_protocol.conf.disabled`
(or something similar) and reload apache2 with `ds reload`. As long as
the file does not end in `.conf`, it will not be loaded in the
configuration of apache2.
