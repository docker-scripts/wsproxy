cmd_create_help() {
    cat <<_EOF
    create
        Create the container '$CONTAINER'.

_EOF
}

rename_function cmd_create _ds_cmd_create
cmd_create() {
    mkdir -p conf.d domains letsencrypt fail2ban
    cp -n $APPDIR/misc/example.conf .
    cp -n $APPDIR/misc/conf.d/* conf.d/
    cp -n $APPDIR/misc/jail.local fail2ban/

    ## fix the subnet at conf.d/proxy_protocol.conf
    #local subnet=$(docker network inspect -f '{{range .IPAM.Config}}{{.Subnet}}{{end}}' $NETWORK)
    #sed -i conf.d/proxy_protocol.conf \
    #    -e "s#172.18.0.0/16#$subnet#"

    _ds_cmd_create \
        --cap-add=NET_ADMIN \
        --mount type=bind,src=$(pwd)/letsencrypt,dst=/etc/letsencrypt \
        --mount type=bind,src=$(pwd)/conf.d,dst=/etc/apache2/conf.d \
        --mount type=bind,src=$(pwd)/domains,dst=/etc/apache2/domains \
        --mount type=bind,src=$(pwd)/fail2ban/jail.local,dst=/etc/fail2ban/jail.local \
        "$@"

    # create global cmd wsproxy and revproxy
    mkdir -p $DSDIR/cmd/
    cp $APPDIR/misc/wsproxy.sh $DSDIR/cmd/
    local wsproxydir=$(basename $(pwd))
    sed -i $DSDIR/cmd/wsproxy.sh \
        -e "s/local wsproxydir=.*/local wsproxydir=$wsproxydir/"
    cp $APPDIR/misc/revproxy.sh $DSDIR/cmd/
}
