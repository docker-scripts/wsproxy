cmd_domains-add_help() {
    cat <<_EOF
    domains-add <domain> [<alias> ...] [-c | --ssl-cert]
         Add a configuration file for the given domain (aliases are optional).
         If the option '--ssl-cert' is given, get a certificate as well.

_EOF
}

cmd_domains-add() {
    local usage="Usage: $COMMAND <domain> [<alias> ...] [-c | --ssl-cert]"

    # get the options
    local ssl_cert=0
    local opts="$(getopt -o c -l ssl-cert -- "$@")"
    local err=$?
    eval set -- "$opts"
    while true; do
        case $1 in
            -c|--ssl-cert) ssl_cert=1; shift ;;
            --) shift; break ;;
        esac
    done
    [[ $err == 0 ]] || fail $usage

    # get the domains
    [[ $# -lt 1 ]] && fail $usage
    local domain=$1
    shift
    local aliases="$@"

    # remove these domains, if they exist
    ds domains-rm $domain $aliases

    # add an apache2 config file for the domain
    cp example.conf domains/$domain.conf
    sed -i domains/$domain.conf \
        -e "s/example\.org/$domain/g"
    # add aliases on the apache2 config file
    [[ -n $aliases ]] && \
        sed -i domains/$domain.conf \
            -e "/ServerName/ a\    ServerAlias $aliases"

    # get a ssl certificate as well, if requested
    [[ $ssl_cert == 1 ]] &&\
        ds get-ssl-cert $domain $aliases

    # reload apache2 config
    ds reload
}
