cmd_domains-ls_help() {
    cat <<_EOF
    domains-ls [<pattern>]
         List the domains that match the given pattern.

_EOF
}

cmd_domains-ls() {
    ls domains/ \
        | grep .conf \
        | sed -e 's/.conf$//' \
        | grep -e "$1"
}
