cmd_get-ssl-cert_help() {
    cat <<_EOF
    get-ssl-cert <domain>... [-t,--test]
         Get free SSL certificates from letsencrypt.org

_EOF
}

cmd_get-ssl-cert() {
    local usage="Usage: $COMMAND <domain>... [-t,--test]"

    # get the options and arguments
    local test=0
    local opts="$(getopt -o t -l test -- "$@")"
    local err=$?
    eval set -- "$opts"
    while true; do
        case $1 in
            -t|--test) test=1; shift ;;
            --) shift; break ;;
        esac
    done
    [[ $err == 0 ]] || fail $usage

    local domains="$@"
    [[ -n $domains ]] || fail $usage

    # build the certbot args
    local args="certonly --webroot --non-interactive --agree-tos --keep-until-expiring"
    args+=" --expand --renew-with-new-domains --allow-subset-of-names"
    args+=" --webroot-path /var/www --email $SSL_CERT_EMAIL"
    args+=" --domains $(echo $domains | tr ' ' , )"
    [[ $test == 1 ]] && args+=" --dry-run"

    # run certbot from inside the container
    ds exec certbot $args

    # if testing, stop here
    [[ $test == 1 ]] && exit 0

    # update config files
    local domain=$(echo $domains | cut -d' ' -f1)
    [[ -f letsencrypt/live/$domain/cert.pem ]] || \
        fail "\nCan not find certificate files for: '$domains'.\nApache config not updated.\n"
    # update apache2 config file in wsproxy
    sed -i domains/$domain.conf -r \
        -e "s|#?SSLCertificateFile.*|SSLCertificateFile      certs/$domain/cert.pem|" \
        -e "s|#?SSLCertificateKeyFile.*|SSLCertificateKeyFile   certs/$domain/privkey.pem|" \
        -e "s|#?SSLCertificateChainFile.*|SSLCertificateChainFile certs/$domain/chain.pem|"

    # reload apache2 config
    ds reload
}
