#!/bin/bash -x

source /host/settings.sh

### redirect http-->https, except for letsencrypt requests
cat <<EOF > /etc/apache2/sites-available/default.conf
<VirtualHost *:80>
    RewriteEngine On
    RewriteCond %{REQUEST_URI} !^/.well-known/acme-challenge [NC]
    RewriteCond %{HTTPS} off
    RewriteRule ^ https://%{HTTP_HOST}%{REQUEST_URI}
</VirtualHost>
EOF
a2ensite default

### configure apache2 for letsencrypt
mkdir -p /var/www/.well-known/acme-challenge/
cat <<EOF > /etc/apache2/conf-available/letsencrypt.conf
Alias /.well-known/acme-challenge /var/www/.well-known/acme-challenge
<Directory /var/www/.well-known/acme-challenge>
    Options None
    AllowOverride None
    ForceType text/plain
</Directory>
EOF
a2enconf letsencrypt

ln -s /etc/letsencrypt/live/ /etc/apache2/certs

### load config files from 'conf.d/' and 'domains/'
cat <<EOF > /etc/apache2/conf-available/include.conf
IncludeOptional conf.d/*.conf
IncludeOptional domains/*.conf
EOF
a2enconf include

# disable proxy_protocol
[[ -z $ENABLE_PROXY_PROTOCOL ]] && \
    mv /etc/apache2/conf.d/proxy_protocol.conf{,.disabled}

### enable apache2 modules
a2enmod ssl cache headers rewrite remoteip \
        proxy proxy_http proxy_wstunnel proxy_connect proxy_balancer

### reload apache2
systemctl reload apache2

### enable fail2ban
systemctl enable fail2ban
systemctl start fail2ban
