cmd_wsproxy_help() {
    cat <<_EOF
    wsproxy [ ls | add | rm | ssl-cert | type | path ]
        Manage the domain '$DOMAIN' of the container.

        ls
            Show whether the domain is being managed by the reverse proxy.

        add
            Add the domain to the configuration of the reverse proxy.

        rm
            Remove the domain from the configuration of the reverse proxy.

        ssl-cert
            Tell the reverse proxy to get a letsencrypt ssl cert for the domain.

        type
            Show the type of the reverse proxy (apache2, nginx, etc.)

        path
            Show the path of the config file for the domain.

_EOF
}

cmd_wsproxy() {
    local wsproxydir=wsproxy
    case $1 in
        ls)        ds @$wsproxydir domains-ls   $DOMAIN  ;;
        add)       ds @$wsproxydir domains-add  $DOMAIN $DOMAINS  ;;
        rm)        ds @$wsproxydir domains-rm   $DOMAIN $DOMAINS  ;;
        ssl-cert)
                   local domain_list
                   for domain in $DOMAIN $DOMAINS; do
                       [[ $domain =~ ^(.*\.)?example\.org$ ]] && continue
                       [[ $domain =~ ^(.*\.)?example\.com$ ]] && continue
                       [[ $domain =~ \.local$ ]] && continue
                       domain_list+=" $domain"
                   done
                   [[ -n $domain_list ]] && ds @$wsproxydir get-ssl-cert $domain_list
                   ;;
	type)      echo 'apache2' ;;
	path)      echo $CONTAINERS/$wsproxydir/domains/$DOMAIN.conf ;;
        *)         echo -e "Usage:\\n$(cmd_wsproxy_help)" ; exit ;;
    esac
}
