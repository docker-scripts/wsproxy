APP=wsproxy
PORTS="80:80 443:443"

SSL_CERT_EMAIL=user@example.org    # needed for getting letsencrypt ssl certs

### Uncomment to enable the proxy_protocol.
#ENABLE_PROXY_PROTOCOL=true
